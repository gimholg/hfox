export interface IURLListerOptions {
  url: string
}

export interface IURLListerResult {
  name: string
  urls: string[]
}

export interface IURLLister<
  Options = IURLListerOptions,
  Result = IURLListerResult
> {
  (opts: Options): Promise<Result>;
}
