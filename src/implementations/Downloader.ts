import fetch from "node-fetch";
import { IDownloader } from "../interfaces/IDownloader";

export const downloader: IDownloader = async (opts) => {
  const resp = await fetch(opts.url);
  const buff = await resp.arrayBuffer();
  const fileName = opts.url.match(/.+\/(.*?)$/)![1];
  return { fileName, buffer: Buffer.from(buff) }
}