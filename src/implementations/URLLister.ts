import fetch from "node-fetch";
import * as cheerio from 'cheerio';
import { IURLLister } from "../interfaces/IURLLister";

export const urlLister: IURLLister = async (opts) => {
  const { url } = opts;
  const response = await fetch(url);
  const buff = await response.arrayBuffer();
  const $ = cheerio.load(Buffer.from(buff));
  const name = url.match(/.+\/(.*?)\.(.*?)$/)![1];
  const urls = Array.from($('[data-lazy-src]')).map(v => v.attribs['data-lazy-src']);
  return { urls, name }
}
