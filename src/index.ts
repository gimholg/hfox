import { program } from 'commander';
import { download } from './download'
import { urlLister, downloader } from './implementations';

const main = (args: any) => {
  const { url } = args
  if (typeof url === 'string')
    return download({ url, lister: urlLister, downloader })
}
program
  .description('download files from url')
  .option('-u, --url [value]', 'url')
  .version('1.0.10', '-v, --version')
  .action(main)
  .parse()