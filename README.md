# hfox

## Installation

```
npm install -g hfox
```

## Usage

```
Usage: hfox usage

download files from url

Options:
  -u, --url [value]  url
  -h, --help         display help for co
```

## Others

my node version: `v16.17.1`

my npm version: `8.15.0`
