/// <reference types="node" />
export interface IDownloadOptions {
    url: string;
}
export interface IDownloadResult {
    fileName: string;
    buffer: Buffer;
}
export interface IDownloader<Options = IDownloadOptions, Result = IDownloadResult> {
    (opts: Options): Promise<Result>;
}
