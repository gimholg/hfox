"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const commander_1 = require("commander");
const download_1 = require("./download");
const implementations_1 = require("./implementations");
const main = (args) => {
    const { url } = args;
    if (typeof url === 'string')
        return (0, download_1.download)({ url, lister: implementations_1.urlLister, downloader: implementations_1.downloader });
};
commander_1.program
    .description('download files from url')
    .option('-u, --url [value]', 'url')
    .version('1.0.10', '-v, --version')
    .action(main)
    .parse();
