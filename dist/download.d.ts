import { IDownloader, IURLLister } from './interfaces';
export declare type Options = {
    url: string;
    lister: IURLLister;
    downloader: IDownloader;
};
export declare function download(opts: Options): Promise<void>;
