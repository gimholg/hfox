"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.download = void 0;
const promises_1 = __importDefault(require("fs/promises"));
const path_1 = __importDefault(require("path"));
function download(opts) {
    return __awaiter(this, void 0, void 0, function* () {
        const { url, downloader, lister } = opts;
        const { urls, name: dir } = yield lister({ url });
        process.stdout.write(`dir: ${path_1.default.join(__dirname, dir)}\n`);
        promises_1.default.mkdir(dir, { recursive: true }).catch(() => { });
        const total = urls.length;
        while (urls.length) {
            const url = urls.splice(0, 1)[0];
            const idx = total - urls.length;
            process.stdout.write(`[${idx}/${total}]${url}`);
            try {
                const { buffer, fileName } = yield downloader({ url });
                const filePath = path_1.default.join(dir, fileName);
                yield promises_1.default.writeFile(filePath, buffer);
                process.stdout.write(' √\n');
            }
            catch (e) {
                process.stdout.write(' ×\n');
                console.trace(e);
                urls.push(url);
            }
        }
    });
}
exports.download = download;
